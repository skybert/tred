package net.skybert.tred;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

/**
 * Low level, socket based Redis integration.
 *
 * @author <a href="mailto:tkj@stibodx.com">Torstein Krause Johansen</a>
 */
public class Main extends Application<SkybertConf> {

  public static void main(final String[] pArgs) throws Exception {
    new Main().run(pArgs);
  }

  @Override
  public void run(
      final SkybertConf pConf,
      final Environment pEnv)
    throws Exception {

    pEnv.jersey().register(new RedisSocketResource(pConf));
  }

}