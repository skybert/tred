
package net.skybert.tred;

import io.dropwizard.Configuration;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * SkybertConf
 *
 * @author <a href="mailto:tkj@stibodx.com">Torstein Krause Johansen</a>
 */
@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class SkybertConf  extends Configuration {
  private String redisHost = "localhost";
  private int redisPort = 6379;
}