package net.skybert.tred;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST API to Redis.
 *
 * @author <a href="mailto:tkj@stibodx.com">Torstein Krause Johansen</a>
 */
@Path("/redis")
public class RedisSocketResource {
  private static final String REDIS_NOT_FOUND_MARKER = "$-1";

  final PrintWriter mRedisOut;
  final BufferedReader mRedisIn;
  int mExpiryInSeconds;

  public RedisSocketResource(
      final SkybertConf pConf)
    throws IOException {

    final Socket clientSocket;
    String ip = pConf.getRedisHost();
    int port = pConf.getRedisPort();
    mExpiryInSeconds = pConf.getExpiryInSeconds();

    clientSocket = new Socket(ip, port);
    mRedisOut = new PrintWriter(clientSocket.getOutputStream(), true);
    mRedisIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

    Thread closeSocketHook = new Thread(() -> {
      System.out.println("Closing Redis socket");
      try {
        clientSocket.close();
      }
      catch (IOException e) {
        e.printStackTrace();
      }
    });

    Runtime.getRuntime().addShutdownHook(closeSocketHook);
  }

  public String set(final String pKey, final String pValue) throws IOException {
    String cmd = String.format(
        "SET %s \"%s\" EX %s",
        pKey,
        pValue.replace("\"", "\\\""),
        mExpiryInSeconds);
    System.out.println(String.format("Redis command: %s", cmd));
    mRedisOut.println(cmd);
    String response = mRedisIn.readLine();
    System.out.println(String.format("Redis said: %s", response));
    return response;
  }

  public String get(final String pKey) throws IOException {
    String cmd = String.format("get %s", pKey);
    System.out.println(String.format("Redis command: %s", cmd));
    mRedisOut.println(cmd);

    // redis answers with two lines. If the client sends:
    // get name
    //
    // redis answers with two lines. The first is the length, the
    // second the actual value:
    // $3
    // foo
    //
    // We only want the second line with the value
    String value = mRedisIn.readLine();
    System.out.println(String.format("Redis length: %s", value));
    if (value.startsWith("$") && !REDIS_NOT_FOUND_MARKER.equals(value)) {
      System.out.println("Reading another line ...");
      value = mRedisIn.readLine();
    }
    else if (REDIS_NOT_FOUND_MARKER.equals(value)) {
      throw new NotFoundException();
    }

    System.out.println(String.format("Redis said: %s", value));
    return value;
  }

  @GET
  @Path("{key}")
  public Response getRedisValue(@PathParam("key") final String pKey)
    throws IOException {

    String response = get(pKey);
    MediaType mediaType = MediaType.TEXT_PLAIN_TYPE;
    if (response.startsWith("{")) {
      mediaType = MediaType.APPLICATION_JSON_TYPE;
    }

    response = response.endsWith("\n") ? response : response + "\n";

    return Response
      .status(Response.Status.OK)
      .type(mediaType)
      .entity(response)
      .build();

  }

  @PUT
  @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
  @Path("{key}")
  public Response putRedisValue(
      @PathParam("key") final String pKey,
      final String pJSON) throws IOException {

    String response = set(pKey, pJSON);
    response = response.endsWith("\n") ? response : response + "\n";

    return Response
      .status(Response.Status.OK)
      .entity(response)
      .build();
  }

}